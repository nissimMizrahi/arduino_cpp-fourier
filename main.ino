#include "fourier.h"
#include "gyro.h"
#define SIGNAL_LEN 15

double _signal[SIGNAL_LEN] = { 0 };  
double _signal2[SIGNAL_LEN] = { 0 };  

Signal freqs[SIGNAL_LEN] = { 0 };

MPU *mpu = NULL;

void print_arr(double* arr, long len)
{
  for(long i = 0; i < len; i++)
  {
    Serial.print(String() + String(arr[i]) + ", ");
  }
  Serial.println();
}

void shift_signal(double *arr, long len)
{
  for(long i = 0; i < len - 1; i++)
  {
    arr[i] = arr[i + 1];
  }
}

void setup()
{
  Serial.begin(9600);
  Serial.println("init");
  mpu = new MPU(0x68);
  Serial.println("ready");
  
  //dft(_signal, freqs, SIGNAL_LEN);
 
  //idft(freqs, _signal, SIGNAL_LEN); 
 
}
void loop()
{
  double x, y, z;
  mpu->getXYZangle(&x, &y, &z);

  _signal[SIGNAL_LEN- 1] = x;

  if(_signal[0] != 0)
  {
    dft(_signal, freqs, SIGNAL_LEN);

    for(long i = 0; i< SIGNAL_LEN; i++)
    {
      if(freqs[i].amp() < 2)
        freqs[i].kill();
    }
    
    idft(freqs, _signal2, SIGNAL_LEN);
  }

  
  shift_signal(_signal, SIGNAL_LEN);
  Serial.println(String(_signal[SIGNAL_LEN - 1]) + " " + String(_signal2[SIGNAL_LEN - 1]));
  //print_arr(_signal, SIGNAL_LEN);
  
  delay(1);
}
