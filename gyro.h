#include <Wire.h>
#include <math.h>

class MPU
{

  int address;
  double _time;
  int grx, gry, grz;
  double pitch, roll;


  public:

  static const int scale = 131;
  
  MPU(int address) : address(address), grx(0), gry(0), grz(0)
  {
    Wire.beginTransmission(address);
    Wire.write(0x6B);// PWR_MGMT_1 register
    Wire.write(0);// (wakesupthe MPU-6050)
    Wire.endTransmission();  
    Wire.beginTransmission(address);
    Wire.write(0x1B);
    Wire.write(0);
    Wire.endTransmission(); 
    Wire.beginTransmission(address);
    Wire.write(0x1C);
    Wire.write(0);
    Wire.endTransmission();

    _time = millis();
  }

  void update()
  {
    Wire.beginTransmission(address);
    Wire.write(0x3B);   
    Wire.endTransmission();
    
    Wire.requestFrom(address,14);  // request a total of 14 registers
    
    double ax = Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
    double ay = Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    double az = Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
    
    int tmp = Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
    
    int gx = Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
    int gy = Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
    int gz = Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
    
    double _timePrev = _time;
    _time = millis();
    double _timeStep = (_time - _timePrev) / 1000; // _time-step in ms

    gx = gx/(5*scale);
    gy = gy/(5*scale);
    gz = gz/(5*scale);

    grx += 5*(_timeStep * gx);
    gry += 5*(_timeStep * gy);
    grz += 5*(_timeStep * gz);
    
    pitch = 180 * atan (ax/sqrt(ay*ay + az*az))/M_PI;
    roll = 180 * atan (ay/sqrt(ax*ax + az*az))/M_PI;
  }

  void getXYZangle(double* x, double* y, double* z)
  {
    update();
    *x = roll;
    *y = pitch;
    *z = grz;
  }

};