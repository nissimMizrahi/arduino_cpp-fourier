#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966192313216916398
#define TWO_PI 6.283185307179586476925286766559

struct Signal
{
  double real;
  double im;
  long freq;

  void mul(double& x)
  {
    this->real *= x;
    this->im *= x;
  }
  void mul_signal(Signal& other)
  {
    double t_real = this->real * other.real - this->im * other.im;
    double t_im = this->real * other.im + this->im * other.real;

    this->im = t_im;
    this->real = t_real;
  }

  void add(Signal& other)
  {
      this->real += other.real;
      this->im += other.im;
  }

  void kill()
  {
    this->real = 0.0;
    this->im = 0.0;
  }

  double amp()
  {
    return sqrt(pow(this->real, 2) + pow(this->im, 2));
  }
  double phase()
  {
    return atan(this->im / (this->real != 0.0 ? this->real : 1.0));
  }
};

void dft(double *input, Signal *output, long N)
{
  for(long k = 0; k < N; k++)
  {
    output[k] = {0, 0, 0};
    
    for(long n = 0; n < N; n++)
    {
      double num = (TWO_PI * k * n) / N;
      
      Signal part = {
        cos(num),
        -sin(num),
        0
      };
      
      part.mul(input[n]);
      output[k].add(part);
    }
    
    double tmp = 1.0 / N;
    output[k].mul(tmp);
    output[k].freq = k;
  }
}

void idft(Signal *input, double *output, long N)
{
  for(long n = 0; n < N; n++)
  {
    Signal sum = {0, 0, 0};
    
    for(long k = 0; k < N; k++)
    {
      double num = (TWO_PI * k * n) / N;
      
      Signal part = {
        cos(num),
        sin(num),
        0
      };
      
      part.mul_signal(input[k]);
      sum.add(part);
    }

    output[n] = sum.real;
  }
}